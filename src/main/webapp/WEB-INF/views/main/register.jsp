
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<style>

.scrollable-menu {
    height: auto;
    max-height: 200px;
    overflow-x: hidden;
}

.modal-dialog .modal-Regi{
    overflow-y: initial !important
}
.modal-regiBody{
    height: 250px;
    overflow-y: auto;
}

</style>
<body>

<div class="modal fade" id="regiModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-Regi">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h1 class="modal-title" id="myModalLabel"><label>Register</label></h1>
      </div>
      <div class="modal-body modal-regiBody">
      
	 <div class="form-group">
	 
	 <div class="inner-addon left-addon form-group">
				<span	class="glyphicon inGlyphicon glyphicon-envelope"></span>
	<input type="email" class="form-control" id="rg_inputEmail" placeholder="Email"> 
	</div>
	 <div class="inner-addon left-addon form-group">
				<span	class="glyphicon inGlyphicon glyphicon-user "></span>
				<input type="text" class="form-control" id="rg_inputName" placeholder="Name">
				</div>
				
				<div class="inner-addon left-addon form-group">
				<span class="glyphicon inGlyphicon glyphicon-lock"></span>
			<input type="password" class="form-control password"
				id="rg_inputPw" placeholder="Password">
				</div>
				
				<div class="inner-addon left-addon form-group">
				<span class="glyphicon inGlyphicon glyphicon-lock"></span>
			<input type="password" class="form-control password"
				id="confirmPw" placeholder="Confirm Password">
				<div class="correctIcon"></div>
				</div>
				
				<div class="inner-addon left-addon form-group">
				<span class="glyphicon inGlyphicon glyphicon-briefcase"></span>
			<input type="text" class="form-control" id="rg_inputCo"
				placeholder="Company">
				</div>
			</div>

			<label>CNC in use</label>
			<div class="btn-group"><ul>
			
			<c:set var="manufacture" value=""/>
		 <c:forEach items="${nc}" var="list">
		 <c:if test="${list.manufacture != manufacture}">
		     <c:set var="manufacture" value="${list.manufacture}"/>
		     </ul>
		     </div>
		     <div class="btn-group">
		     <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        ${manufacture} <span class="caret"></span>
        </button>
        <ul class="dropdown-menu scrollable-menu" role="menu">
		 </c:if>
	   <li><a href="#" data-value="${list.id}"><input type="checkbox"/> ${list.model}</a></li>
	   </c:forEach>  
	   </ul></div>
  <div class="well">
 <table class="table" id="ncTable">
  <tr>
  <th>Manufacture</th>
  <th>Model</th>
  <th></th>
  </tr>
  <tr>
  <td><input type="text" class="form-control direct" placeholder="Manufacture" id="ncCompany"></td>
  <td><input type="text" class="form-control direct" placeholder="Model" id="ncModel"></td>
  <td><span class="glyphicon glyphicon-plus"></span></td>
  </tr>
</table>
</div>

  </div> <!-- end of panel body -->
<div class="modal-footer">
			<center><button class="btn btn-lg btn-primary" onclick="enter()">Register</button></center>
</div> <!-- end of panel footer -->
</div>
</div>
</body>

<script>
	$(document).ready(function() {
						$('.password').keyup(function() {
							if ( $('.password').val().length < 6 ){
								$('.correctIcon').html(
										'<span class="glyphicon glyphicon-remove-sign" style="color:red"> Enter over six-letter password.</span>');
							}
							else if ( $('#inputPw').val()==$('#confirmPw').val()) {
												$('.correctIcon').html(
														'<span class="glyphicon glyphicon-ok-sign" style="color:green"> Correct</span>');
											} else {
												$('.correctIcon').html(
														'<span class="glyphicon glyphicon-remove-sign" style="color:red"> Enter same password.</span>');
											}
							});
					});
	
	var options = [];
	$( '.dropdown-menu a' ).on( 'click', function( event ) {
	   var $target = $( event.currentTarget ),
	       val = $target.attr( 'data-value' ),
	       $inp = $target.find( 'input' ),
	       idx;
	   if ( ( idx = options.indexOf( val ) ) > -1 ) {
	      options.splice( idx, 1 );
	      setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
	   } else {
	      options.push( val );
	      setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
	   }
	   $( event.target ).blur();
	   console.log( options );
	   return false;
	});
	
	var table = document.getElementById("ncTable");
	
	$(".glyphicon-plus").click(function(){
		var row = table.insertRow($(this).closest('tr').index());
		var manufacture = row.insertCell(0);
		var model = row.insertCell(1);
		var deleteIcon = row.insertCell(2);
		manufacture.innerHTML = $("#ncCompany").val();
		model.innerHTML = $("#ncModel").val();
		deleteIcon.innerHTML = "<span class='glyphicon glyphicon-minus' onclick='deleteRow(this)'></span>";
		$(".direct").val('');
		//console.log($(this).closest('tr').index());
	});
	
	function deleteRow(x){
		table.deleteRow($(x).closest('tr').index());
	}
	
	// login modal
	function enter(){
		$("#regiModal").modal('hide');
		$('#myModal').modal('show');
	}

	// register modal
	function register(){
		var userInfo = {};
		
		if($('#rg_inputEmail').val()){}
		
	 $('#myModal').modal('hide');
		$("#regiModal").modal('show'); 
	}
</script>
</html>