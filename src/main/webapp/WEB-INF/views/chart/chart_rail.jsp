<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">


<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
	
	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
</script>
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery-ui.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.circle-diagram.js"></script>
<script src="${ctxPath }/js/jqMap.js"></script>
<script src="${ctxPath }/js/chart/highstock_data.js"></script>	
<script type="text/javascript" src="${ctxPath }/js/highstock.js"></script>
<script type="text/javascript" src="${ctxPath }/js/highcharts-3d.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/bookblock.css" />
<script src="${ctxPath }/js/multicolor_series.js"></script>
<script src="${ctxPath }/js/modernizr.custom.js"></script>
<script src="${ctxPath }/js/jquery.bookblock.js"></script>
<script src="${ctxPath }/js/card_rail.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/card.css">
<style type="text/css">
	body{
		overflow: hidden;
	}
</style>
</head>

<body >
	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
	</form>
	
	<center>
		<img alt="" src="${ctxPath }/images/unos.png" style="opacity:0" id="unos">
	</center>
		
	<div id="panel">
		<table id="panel_table" width="100%">
			<tr align="center">
				<td colspan="2"><p id="comName">Application</p></td>
			</tr>
			<tr align="center">
				<td width="50%">
					<img alt="" src="${ctxPath }/images/menu/rail.png" class="menu_icon" id="DashBoard_1" draggable="false">
					<p>레일연삭</p>
				</td>
				<td>
					<img alt="" src="${ctxPath }/images/menu/block.png" class="menu_icon" id="Block" draggable="false">
					<p>블럭가공</p>
				</td>
			</tr>
			<tr align="center">
				<td>
					<img alt="" src="${ctxPath }/images/menu/report.png" class="menu_icon" id="Report_3" draggable="false">
					<p>통계</p>
				</td>
				<td>
					<img alt="" src="${ctxPath }/images/menu/history.png" class="menu_icon" id="history" draggable="false">
					<p>히스토리</p>
				</td>				
			</tr>
		</table>
	</div>
	<div id="corver"></div>
	<img alt="" src="${ctxPath }/images/myApps.png" id="menu_btn">
			<!-- Part1 -->
			<div id="part1" class="page" >
				<img alt="" src="${ctxPath }/images/close_btn.png" id="close_btn" style="position: absolute; opacity:0">
				<div id="machineListForTarget" style="opacity:0">
					<center>
						<table id="machineListTable" style="width: 100%">
						</table>
					</center>
				</div>
				
				<div id="monthlyTargetBox">
					<input type="text" id="monthlyTargetValue" placeholder="월 목표량"><span id="monthly_target_save_btn">확인</span>
				</div>
				<img id="edit_monthly_target" src="${ctxPath }/images/edit.png" style="display: none" class='edit'>
				<img id="edit_daily_target" src="${ctxPath }/images/edit.png" style="display: none" class='edit'>
				 
				<div id="monthlyTarget">
					월 목표량 : 0m
				</div>
				<div id="grid"></div>
				<div class="ghost-select"><span></span></div>
				
				
				<div id="svg">
				</div>
						
				<div id="admin_pwd_box">
					 <center>비밀번호</center>
					 <br>
					<input type="password" id="pwd">
				</div>
				
				<img alt="" src="${ctxPath }/images/logo.png" id="main_logo" style="display: none">
				<table id="time_table">
					<tr>
						<td id="today"></td>
					</tr>
				</table>
				<div id="statusBox">
					<div id="status_pie" style="float: left;"></div>
					<div id="status_pie2" style="float: right;"></div>
				</div>
				<div class="container">
					<div id="barChart"></div>
					<table id="main_table" style="border-collapse: collapse; text-align: center; display: none"  >
					</table>
				<canvas id="canvas" width="200" height="200"></canvas>
				<div id="stateBorder"></div>
					<img alt="" src="${ctxPath }/images/map.svg" id="map">
					<center>
						<table class="mainTable" id="mainTable">
							<tr>
								<Td align="center" style="background-color:rgb(34,34,34); font-weight: bolder;" class="title" id="title_main">
										장비 가동 현황
								</Td>
							</tr>
						</table>
						
						<div id="cards">
						
						</div>
					</center>
				</div>
			</div>		
	
	
	<!-- Part2 -->
	<div id="part3" class="page" style="background-color: rgb(16, 18, 20)">
		<center>
			<table class="mainTable" id="reportTable"style="background-color: rgb(16, 18, 20)" >
				<tr>
					<Td align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="title"  id="part3_title">
							통계
					</Td>
				</tr>
				<tr >
					<td align="center"  style="background-color:rgb(34,34,34);color:rgb(34,34,34) "class="title part3_no_padding">
						<img alt="" src="${ctxPath }/images/excel.svg" id="excel">
							<div id="reportDateDiv">
								<input type="date" id="sDate"> - 
								<input type="date" id="eDate">
							</div>
					</td>
				</tr>
				<Tr align="center">
					<td style="background-color:rgb(34,34,34); padding: 0" class="title part3_no_padding" id="tableTd" valign="top">
						<div id="table_wrapper">
							<table id="tableDiv" style="width: 100%; background-color: rgb(34,34,34); border-collapse: collapse; padding: 0; margin: 0" class="subTable" border="1" >
							</table>
						</div>
					</td>
				</Tr>
			</table>
		</center>
	</div>
</body>
</html>