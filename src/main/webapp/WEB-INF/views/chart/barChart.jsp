<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">

<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
	
	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
</script>
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery-ui.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.circle-diagram.js"></script>
<script src="${ctxPath }/js/jqMap.js"></script>
<script src="${ctxPath }/js/chart/highstock_data.js"></script>	
<script type="text/javascript" src="${ctxPath }/js/highstock.js"></script>
<script type="text/javascript" src="${ctxPath }/js/highcharts-3d.js"></script>
<script src="${ctxPath }/js/multicolor_series.js"></script>
<script src="${ctxPath }/js/barChart.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/card.css">
</head>

<body oncontextmenu="setSlideMode(); return false">
	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
	</form>
	
	<div id="panel">
		<table id="panel_table" width="100%">
			<tr align="center">
				<td colspan="2"><p id="comName">MENU</p></td>
			</tr>
			<tr align="center">
				<td width="50%">
					<img alt="" src="${ctxPath }/images/menu/board.png" class="menu_icon" id="DashBoard_1" draggable="false">
					<p>가공 현황</p>
				</td>
				<!-- <td>
					<div  class="menu_icon" id="Report_4" draggable="false" style="background-color: blue"></div>
					<p>블럭가공</p>
				</td> -->
				<td>
					<img alt="" src="${ctxPath }/images/menu/report.png" class="menu_icon" id="Report_3" draggable="false">
					<p>통계</p>
				</td>
			</tr>
		</table>
	</div>
	<img alt="" src="${ctxPath }/images/logo.png" id="main_logo" style="display: none">
		<table id="time_table">
			<tr>
				<td id="today"></td>
			</tr>
		</table>
	<div id="corver"></div>
	<%-- <img alt="" src="${ctxPath }/images/myApps.png" id="menu_btn"> --%>
	
	<!-- Part1 -->
	<div id="part1" class="page" style="background-color:  rgb(16, 18, 20)">
		<table class="mainTable" >
					<tr>
						<Td align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="title" colspan="2">
							장비 가동현황
						</Td>
					</tr>
					<tr>
						<td width="50%">
							<div id="chart1"></div>
						</td>
						<td>
							<div id="chart2"></div>
						</td>
					</tr>
		</table>
	</div>
</body>
</html>