<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">

<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
	
	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
</script>
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery-ui.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.circle-diagram.js"></script>
<script src="${ctxPath }/js/jqMap.js"></script>
<script src="${ctxPath }/js/chart/highstock_data.js"></script>	
<script type="text/javascript" src="${ctxPath }/js/highstock.js"></script>
<script type="text/javascript" src="${ctxPath }/js/highcharts-3d.js"></script>
<script src="${ctxPath }/js/multicolor_series.js"></script>
<script src="${ctxPath }/js/detailChart_rail_demo.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/card.css">
</head>

<body oncontextmenu="setSlideMode(); return false">
	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
	</form>
	
	<div id="panel">
		<table id="panel_table" width="100%">
			<tr align="center">
				<td colspan="2"><p id="comName">MENU</p></td>
			</tr>
			<tr align="center">
				<td width="50%">
					<img alt="" src="${ctxPath }/images/menu/board.png" class="menu_icon" id="DashBoard_1" draggable="false">
					<p>가공 현황</p>
				</td>
				<!-- <td>
					<div  class="menu_icon" id="Report_4" draggable="false" style="background-color: blue"></div>
					<p>블럭가공</p>
				</td> -->
				<td>
					<img alt="" src="${ctxPath }/images/menu/report.png" class="menu_icon" id="Report_3" draggable="false">
					<p>통계</p>
				</td>
			</tr>
		</table>
	</div>
	<img alt="" src="${ctxPath }/images/logo.png" id="main_logo" style="display: none">
		<table id="time_table">
			<tr>
				<td id="today"></td>
			</tr>
		</table>
		
	<div id="toolBox">
		공구1 (11:30)<br>
		공구2 - Critical Error (14:30)
	</div>
	<div id="alarmBox">
		A34 - Power Off (11:30)<br>
		B94 - Critical Error (14:30)
	</div>
	<div id="repairBox">
		수리이력 내역 (10:30)<br>
	</div>
	<div id="corver"></div>
	<%-- <img alt="" src="${ctxPath }/images/myApps.png" id="menu_btn"> --%>
	
	<!-- Part1 -->
	<div id="part1" class="page" style="background-color:  rgb(16, 18, 20)">
		<div id="mainTable2">
			<center>
				<table class="mainTable" >
					<tr>
						<Td align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="title" colspan="6">
							개별 장비 가동 현황
						</Td>
					</tr>
					<Tr class="tr1" >
						<td colspan="4" width="60%" rowspan="2" valign="top">
						<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle">
								상태	
							</div>
							<div id="container"></div>
						</td>
						<td width="40%" valign="top" id="machine_name_td" colspan="2">
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle">
							장비	<img alt="" src="${ctxPath }/images/tool.png" id="tool">
							</div>
							<div id="machine_name">

							</div>
						</td>		
					</Tr >
					<tr class="tr1">
						<td valign="top" id="alarm_td" colspan="2">
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle">
							알람
							</div>
							<div id="alarm" align="left">

							</div>
						</td>
						
					</tr>
					<tr class="tr2" valign="top" id="chartTr">
						<td width="15%">
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle">
							 	In-Cycle (분)
							</div>	
							<div class="neon1" id="inCycleTime">
							 </div>
						</td>
						<td width="15%">
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle">
								Wait (분)
							</div>
							<div class="neon2" id="waitTime">
							</div>
						</td>
						<td width="15%" >
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle">
							Alarm (분)
							</div>
							<div class="neon3" id="alarmTime">
							</div>
						</td>
						<td width="15%">
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle">
							No-Conncection (분)
							</div>
							<div class="neon3" id="noConnTime">
							</div>
						</td>
						<td width="20%">
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle">
						Feed Override
							</div>
							<div class="neon3"   id="feedOverride">
							</div>
						</td>
						<td valign="top" width="20%">
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle" >
								Spindle Load		
							</div>
							<div  class="neon3" id="spdLoad">
							</div>
						</td>
					</tr>
					<tr>
						<Td align="center" style="color:white; font-weight: bolder;" class="title" colspan="6" id="lastCell">
							<center>
								<div id="barChart"></div>
							</center>							
						</Td>
					</tr>
				</table>
			</center> 
		</div>
	</div>
</body>
</html>