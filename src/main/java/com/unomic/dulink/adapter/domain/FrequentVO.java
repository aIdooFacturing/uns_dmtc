package com.unomic.dulink.adapter.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class FrequentVO {
	
	int IDX;
	int dvcId;
	String start_date_time;
	String end_date_time;
	float spd_ld;
	int spd_act_speed;
	int act_fd;
	float axis_ld_z;
	String REGTIME;
	
	int count;
	
}
