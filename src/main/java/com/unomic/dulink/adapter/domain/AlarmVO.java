package com.unomic.dulink.adapter.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AlarmVO {

	String alarmCode;
	int dvcId;
	int IDX;
	String alarmMsg;
	String start_date_time;
	String end_date_time;
	
}
