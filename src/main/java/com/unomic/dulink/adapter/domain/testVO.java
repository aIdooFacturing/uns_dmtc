package com.unomic.dulink.adapter.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class testVO {

	int dvcId;
	String sendDateTime;
	String startDateTime;
	
}
