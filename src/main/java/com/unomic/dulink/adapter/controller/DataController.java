package com.unomic.dulink.adapter.controller;

import java.text.SimpleDateFormat;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.adapter.service.DataService;

@Controller
@RequestMapping("/adapter")
public class DataController {

	@Autowired
	DataService adapterService;

	@RequestMapping("/setRareData")
	@ResponseBody
	public String setRareData(@RequestBody String strJson) {
		String result = "";
		try {
			result = adapterService.setRareData(strJson);
		} catch (Exception e) {
			result = "fail";
		}
		return result;
	}

	@RequestMapping("/setLampData")
	@ResponseBody
	public String setLampData(@RequestBody String strJson) {
		String str = "";
		try {
			str = adapterService.setLampData(strJson);
		} catch (Exception e) {
			str = "fail";
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping("/getLampData")
	@ResponseBody
	public String getContact(String dvcId) {
		String str = "";
		try {
			str = adapterService.getContact(dvcId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			str = "fail";
		}
		return str;
	}

	@RequestMapping("/setFrequentData")
	@ResponseBody
	public String setFrequentData(@RequestBody String strJson) {
		String str = "";
		try {
			str = adapterService.setFrequentData(strJson);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			str = "fail";
		}
		return str;
	}

	@RequestMapping("/getInitTime")
	@ResponseBody
	public String getInitTime() {
		
		String str = "";
		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.HOUR, 8);
		str = dayTime.format(cal.getTime());
		System.out.println("중국 시간 : " + str);

		return str;
	}

}
