package com.unomic.dulink.adapter.service;

public interface DataService {

	String setRareData(String strJson) throws Exception;

	String getContact(String strJson) throws Exception;

	String setLampData(String val) throws Exception;

	String setFrequentData(String strJson)throws Exception;

}
