package com.unomic.dulink.program.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.program.domain.ProgramVo;
import com.unomic.dulink.program.service.ProgramService;
/**
 * Handles requests for the application home page.
 */ 
@RequestMapping(value = "/prog") 
@Controller
public class ProgramController {

	private static final Logger logger = LoggerFactory.getLogger(ProgramController.class);
	/**
	 * Simply selects the home view to render by returning its name.
	 */ 
	@Autowired
	private ProgramService progService;

	@RequestMapping(value="getJobList")
	@ResponseBody
	public String getJobList(ProgramVo progVo){
		String str = "";
		try {
			str = progService.getJobList(progVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	};
	
	@RequestMapping(value="index")
	public String index(){
		return "mobile/index";
	};
	
	@RequestMapping(value="playList")
	public String playList(HttpServletRequest request){
		request.setAttribute("id", request.getParameter("id"));
		return "mobile/playList";
	};
	
	@RequestMapping(value="getSelectedMahcineInfo")
	@ResponseBody
	public ChartVo getSelectedMahcineInfo(ChartVo chartVo){
		try {
			chartVo = progService.getSelectedMahcineInfo(chartVo);	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return chartVo;
	};
};

