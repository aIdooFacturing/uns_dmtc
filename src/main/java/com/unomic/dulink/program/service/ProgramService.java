package com.unomic.dulink.program.service;

import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.program.domain.ProgramVo;

public interface ProgramService {
	public ChartVo getSelectedMahcineInfo(ChartVo chartVo) throws Exception;
	public String getJobList(ProgramVo progVo) throws Exception;
};

