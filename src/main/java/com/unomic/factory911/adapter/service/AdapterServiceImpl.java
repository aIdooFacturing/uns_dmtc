package com.unomic.factory911.adapter.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.factory911.adapter.domain.AdapterVo;
import com.unomic.factory911.device.domain.DeviceStatusVo;
import com.unomic.factory911.device.domain.DeviceVo;

@Service
@Repository
public class AdapterServiceImpl implements AdapterService{

	private final static String ADAPTER_SPACE= "com.factory911.adapter.";
	private final static String DEVICE_SPACE= "com.factory911.device.";
	private static final Logger logger = LoggerFactory.getLogger(AdapterServiceImpl.class);
	
	@Autowired
	@Resource(name="sqlSession_ma")
	private SqlSession sql_ma;
	

	@Override
	public AdapterVo getLastInputData(AdapterVo inputVo){

		AdapterVo newVo = new AdapterVo();
		newVo.setDvcId(inputVo.getDvcId());

		 Integer cnt =  (Integer) sql_ma.selectOne(ADAPTER_SPACE + "cntAdapterStatus",inputVo);
		//Integer cnt =  (Integer) sql_ma.selectOne(ADAPTER_SPACE + "fastCntAdapterStatus",inputVo);
		 logger.info("cnt:"+cnt);

		if(0 < cnt){
		// if( cnt == null || cnt != 1 ){
			newVo  = (AdapterVo) sql_ma.selectOne(ADAPTER_SPACE + "getLastAdapterStatus",inputVo);
		}
		logger.info("newVo:"+newVo);
    	return newVo;
	}
	
	@Override
	public AdapterVo chkDateStarterAGT(AdapterVo preVo, AdapterVo crtVo){
		if(CommonFunction.isDateStart(preVo.getStartDateTime(), crtVo.getStartDateTime())){
			logger.error("@@@@@start Date Time AGT@@@@@");
			logger.error("startDateTime:"+preVo.getStartDateTime());
			logger.error("endDateTime:"+crtVo.getStartDateTime());
			AdapterVo starterVo = new AdapterVo();
			starterVo.setDvcId(crtVo.getDvcId());
			starterVo.setStartDateTime(CommonFunction.getStandardHourToday());
			starterVo.setChartStatus(CommonCode.MSG_DATE_START);
			starterVo.setWorkDate(CommonFunction.dateTime2WorkDate(CommonFunction.getStandardHourToday()));

			editLastEndTime(starterVo);
			addPureStatus(starterVo);
			
			return starterVo;
		}
		return null;
		
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String editLastEndTime(AdapterVo firstOfListVo)
	{
		int cnt = (Integer) sql_ma.selectOne(ADAPTER_SPACE + "cntLastAdapterStatus", firstOfListVo);
		//int cnt = (Integer) sql_ma.selectOne(ADAPTER_SPACE + "fastCntLastAdapterStatus", firstOfListVo);
		if(0 < cnt)
		{
			AdapterVo tmpAdapterVo = (AdapterVo) sql_ma.selectOne(ADAPTER_SPACE + "getLastStartTime", firstOfListVo);
			tmpAdapterVo.setSender(firstOfListVo.getSender());
			//이루틴에 안들어감..추후 보완 필요.?
			if(cnt > 1){
				sql_ma.delete(ADAPTER_SPACE + "removeExceptionEndTime",tmpAdapterVo);
			}
			
			tmpAdapterVo.setEndDateTime(firstOfListVo.getStartDateTime());

			sql_ma.update(ADAPTER_SPACE + "editLastEndTime", tmpAdapterVo);
			
		}

		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String addPureStatus(AdapterVo pureStatusVo)
	{
		logger.info("addPureStatus:"+pureStatusVo.getDvcId());
		sql_ma.insert(ADAPTER_SPACE+"addPureData", pureStatusVo);
		return "OK";
	}
	
	@Override
	@Transactional(value="txManager_ma")
	public String addListPureStatus(List<AdapterVo> listPureStatus)
	{	
		HashMap <String, Object> dataMap = new HashMap<String, Object>();
		
		dataMap.put("listPureStatus", listPureStatus);
		sql_ma.insert(ADAPTER_SPACE + "addListPureData", dataMap);

		return "OK";
	}
	
	//두개의 다른 기능이지만 트랜잭션(동기화?) 때문에 하나로 묶어둠.
	@Override
	@Transactional(value="txManager_ma")
	public String editLastNAddList(List<AdapterVo> listPureStatus, List<DeviceStatusVo> dvcList)
	{
		if(listPureStatus.size()<1){
			return "ZeroSize";
		}
		AdapterVo firstOfListVo = listPureStatus.get(0);
		
		//기존 데이터 수량 체크해서 0일 경우에 는 아무것도 안함. 0 이 아니면 udpate.
		int cnt = (Integer) sql_ma.selectOne(ADAPTER_SPACE + "cntLastAdapterStatus", firstOfListVo);
		//int cnt = (Integer) sql_ma.selectOne(ADAPTER_SPACE + "fastCntLastAdapterStatus", firstOfListVo);
		if(cnt > 0){
			//remove Time Exception Data
			//When transaction lock case.
			logger.info(">>>>>LPS STEP1>>>>>"+listPureStatus.get(0));
			int cntExc = (Integer) sql_ma.selectOne(ADAPTER_SPACE + "cntExceptionData", firstOfListVo);
			if(cntExc>0){
				logger.error("!CntExc!:["+cntExc+"]//ID:["+firstOfListVo.getDvcId()+"]//STDT:["+firstOfListVo.getStartDateTime()+"]");
			}
			//sql_ma.delete(ADAPTER_SPACE+"removeExceptionData", firstOfListVo);
	
			
			firstOfListVo.setDvcId(firstOfListVo.getSender());
			AdapterVo tmpAdapterVo = (AdapterVo) sql_ma.selectOne(ADAPTER_SPACE + "getLastStartTime", firstOfListVo);
			if(tmpAdapterVo!=null){
				logger.info("tmpAdapterVo:"+tmpAdapterVo);
				logger.info("firstOfListVo:"+firstOfListVo);
				tmpAdapterVo.setSender(firstOfListVo.getSender());
				tmpAdapterVo.setEndDateTime(firstOfListVo.getStartDateTime());
				if(CommonCode.isTest){}
				else{
					sql_ma.update(ADAPTER_SPACE + "editLastEndTime", tmpAdapterVo);
				}
			}
		}
		
		HashMap <String, List<AdapterVo>> dataMap = new HashMap<String, List<AdapterVo>>();
		dataMap.put("listPureStatus", listPureStatus);
		
		logger.info(">>>>>LPS STEP2>>>>>"+listPureStatus.get(0));
		if(CommonCode.isTest){}
		else{
			sql_ma.insert(ADAPTER_SPACE + "addListPureData", dataMap);
		}
		
		/*
		 * 알람 리스트 생성
		 */
		try{
			HashMap <String, List<DeviceStatusVo>> dataMapDvc = new HashMap<String, List<DeviceStatusVo>>();
			DeviceStatusVo inputVo = dvcList.get(0);
			int cntDvc = (int) sql_ma.selectOne(DEVICE_SPACE +"cntLastDvcStatus", inputVo);
			Iterator <DeviceStatusVo> it =  dvcList.iterator();
			
			
			//상태 비교용 리스트
			List<DeviceStatusVo> finalList = new ArrayList<DeviceStatusVo>();
			
			//예전 데이터가 있을 경우 가져와서 비교 대상으로 넣는다.
			logger.error("cntDvc:"+cntDvc);
			if(cntDvc>0)
			{
				DeviceStatusVo lastVo = (DeviceStatusVo) sql_ma.selectOne(DEVICE_SPACE +"getLastDvcStatus", inputVo);
				finalList.add(lastVo);
				logger.error("lastVo:"+lastVo);
			}
			
			while(it.hasNext()){
				DeviceStatusVo crtDvcVo = it.next();
				DeviceStatusVo lastDvcVo;

				//예전 데이터가 없으면 바로 리스트에 넣기.
				//이런 경우는 cntDvc가 0인 경우, 즉 예전 데이터가 하나도 없을 경우만.
				if(finalList.size() == 0){
					 finalList.add(crtDvcVo);
				}
				
				//리스트의 마지막 데이터를 가져와서 비교 대상으로 놓는다.
				lastDvcVo = finalList.get(finalList.size()-1);
				
				//연속적인 알람을 구분하기 위해 in-cycle, wait, 등 다른 상태도 리스트에 같이 들어감.연속적이지만 않으면 됨.
				if(crtDvcVo.getChartStatus().equals(lastDvcVo.getChartStatus())){
					continue;
				}else{
					//status가 다름.
					logger.error("crtDvcVo:"+crtDvcVo);
					finalList.add(crtDvcVo);
				}
			}
			
			logger.error("cntDvc:"+cntDvc);
		// 예전 데이터 있을 경우에 endTime update.
			if(cntDvc > 0){
				DeviceStatusVo lastVo = finalList.get(0);
				logger.error("check step1");
				if(finalList.size()>1){
					lastVo.setEndDateTime(finalList.get(1).getStartDateTime());
					sql_ma.update(DEVICE_SPACE + "editLastDvcStatus", lastVo );
				}
				finalList.remove(0);
			}
			
			logger.error("finalList.size():"+finalList.size());
			if(finalList.size()>0){
				logger.error("check step2");
				logger.error("lastVo:"+finalList.get(finalList.size()-1));
				
				dataMapDvc.put("listDvcStatus", finalList);
				sql_ma.insert(DEVICE_SPACE + "addListDvcStatus", dataMapDvc);
			}
			
		}catch(Exception e){
			logger.error(e.toString());
		}
		
//		try{
//		// 알람 리스트 필요해서 추가. 
//		// 쿼리 마이그레이션 안되서 일단 주석화.
//		HashMap <String, List<DeviceStatusVo>> dataMapDvc = new HashMap<String, List<DeviceStatusVo>>();
//		DeviceStatusVo inputVo = dvcList.get(0);
//		
//		int cntDvc = (int) sql_ma.selectOne(DEVICE_SPACE +"cntLastDvcStatus", inputVo);
//		Iterator <DeviceStatusVo> it =  dvcList.iterator();
//		
//		if(cntDvc < 1){//대상 장비 누적값 없을경우 바로 추가.
//			dataMapDvc.put("listDvcStatus", dvcList);
//			sql_ma.insert(DEVICE_SPACE + "addListDvcStatus", dataMapDvc);
//		}else{// 기존값 있을경우 중복 체크해서 결정.
//			// 마지막 값 찾아옴
//			
//			DeviceStatusVo lastVo = (DeviceStatusVo) sql_ma.selectOne(DEVICE_SPACE +"getLastDvcStatus", inputVo);
//			logger.info("lastVo:"+lastVo);
//			
//			// Compare with finalList and dvcList.
//			List<DeviceStatusVo> finalList = new ArrayList<DeviceStatusVo>();
//			finalList.add(lastVo);
//			
//			while(it.hasNext()){
//				DeviceStatusVo crtDvcVo = it.next();
//				DeviceStatusVo lastDvcVo = finalList.get(finalList.size()-1);
//				
//				//연속적인 알람을 구분하기 위해 in-cycle, wait, 등 다른 알람도 넣음.
//				if(crtDvcVo.getChartStatus().equals(lastDvcVo.getChartStatus())
//					){
//					continue;
//				}else{
//					//status가 다름.
//					lastDvcVo.
//					
//					finalList.add(crtDvcVo);
//				}
//			}
		
//			//비교후 0번은 제거.
//			finalList.remove(0);
//			
//			if(finalList.size()>0){
//				dataMapDvc.put("listDvcStatus", finalList);
//				logger.info("finalList.get(0):"+finalList.get(0));
//				//sql_ma.update(DEVICE_SPACE + "setEndTmDvcStatus", finalList.get(0) );
//				sql_ma.update(DEVICE_SPACE + "editLastDvcStatus", finalList.get(0) );
//				sql_ma.insert(DEVICE_SPACE + "addListDvcStatus", dataMapDvc);
//			}
//		}
//		}catch(Exception e){
//			logger.error("device_status:"+e.toString());
//		}
		
		return "OK";
	}
	
	
	@Override
	public List<AdapterVo> getListWinAgent()
	{
		List<AdapterVo> rtnList  = sql_ma.selectList(ADAPTER_SPACE + "getListWinAgent");
		return rtnList;
	}
	
	private AdapterVo isDuple(AdapterVo preVo, AdapterVo inputVo){
		Long preStartTime = 0L;
		if(null == preVo.getStartDateTime()){
			preStartTime = CommonFunction.dateTime2Mil(preVo.getStartDateTime());
		}else{
			preStartTime = CommonFunction.dateTime2Mil(inputVo.getStartDateTime());
		}
		Long crtStartTime = CommonFunction.dateTime2Mil(inputVo.getStartDateTime());
		
		if(crtStartTime<preStartTime){
			logger.error("timeTrash data");
			return null;
		}

		if(preVo.getAlarmNum1().equals(inputVo.getAlarmNum1())
			&&preVo.getAlarmNum2().equals(inputVo.getAlarmNum2())
			&&preVo.getAlarmNum3().equals(inputVo.getAlarmNum3())
			&&preVo.getFdOvrd().equals(inputVo.getFdOvrd())
			&&preVo.getMode().equals(inputVo.getMode())
			&&preVo.getIsZeroSpdLoad().equals(inputVo.getIsZeroSpdLoad())
			&&preVo.getIsZeroFdOvrd().equals(inputVo.getIsZeroFdOvrd())
			&&preVo.getIsZeroActFd().equals(inputVo.getIsZeroActFd())
			&&preVo.getPrgmHead().equals(inputVo.getPrgmHead())
			&&preVo.getMainPrgmName().equals(inputVo.getMainPrgmName())
			&&preVo.getMdlM1().equals(inputVo.getMdlM1())
			&&preVo.getMdlM2().equals(inputVo.getMdlM2())
			&&preVo.getMdlM2().equals(inputVo.getMdlM3())
		)
		{
			logger.info("Duple");
	    	return null;
		}else{
    		return inputVo;
		}
	}

	public String editLastDvcStatus(DeviceVo inputVo)
	{
		logger.error("RUN editLastDvcStatus:"+inputVo);
		sql_ma.update(DEVICE_SPACE + "editDvcLastStatus", inputVo);
	
		return "OK";
	}
	
	
	
	private String genJsonAlarm(String strAlarm1, String strAlarm2){
		//private String genJsonAlarm(String alarmCode1, String alarmMsg1, String alarmCode2,String alarmMsg2){
			//[{"alarmMsg":"FEED SWITCH 0%","alarmCode":"2204"},{"alarmMsg":"FEED SWITCH 0%","alarmCode":"2204"}]
			logger.info("strAlarm1:"+strAlarm1);
			logger.info("strAlarm2:"+strAlarm2);
			
			JSONArray innerArray = new JSONArray();
			JSONObject innerObject1 = new JSONObject();
			JSONObject innerObject2 = new JSONObject();
			
			String[] arrAlarm1;
			String[] arrAlarm2;
			String alarmMsg1="";
			String alarmCode1="";
			String alarmMsg2="";
			String alarmCode2="";
			if(strAlarm1==null ){

			}else if(!strAlarm1.isEmpty()){
				arrAlarm1 = strAlarm1.split(" ");
				alarmCode1 = arrAlarm1[0];
				alarmMsg1 = arrAlarm1[1];
			}
			if(strAlarm2==null ){
				
			}else if(strAlarm2==null || !strAlarm2.isEmpty()){
				arrAlarm2 = strAlarm2.split(" ");
				alarmCode2 = arrAlarm2[0];
				alarmMsg2 = arrAlarm2[1];
			}

			
			innerObject1.put("ALARM_MSG", alarmMsg1);
			innerObject2.put("ALARM_MSG", alarmMsg2);
			
			innerObject1.put("ALARM_CODE", alarmCode1);
			innerObject2.put("ALARM_CODE", alarmCode2);
			
			innerArray.add(innerObject1);
			innerArray.add(innerObject2);
			
			return innerArray.toString();
		}
		
		private String genJsonModal(String modal){
			String[] arrModal = modal.split(" ");
			List<String> arrM = new ArrayList<String>();
			int size = arrModal.length;
			
			String gCode = "";
			String tCode = "";
			for(int i = 0 ; i<size ; i++){
				
				if(arrModal[i].charAt(0)=='G'){
					gCode = arrModal[i].substring(1);
				}else if(arrModal[i].charAt(0)=='T'){
					tCode = arrModal[i].substring(1);
				}else if(arrModal[i].charAt(0)=='M'){
					arrM.add(arrModal[i].substring(1));
				}
			}

			JSONObject rootObj = new JSONObject();
			JSONArray arrGmodal = new JSONArray();
			JSONObject objG0 = new JSONObject();
			JSONArray arrAuxCode= new JSONArray();
			JSONObject objM1 = new JSONObject();
			JSONObject objM2 = new JSONObject();
			JSONObject objM3 = new JSONObject();
			JSONObject objT = new JSONObject();
			
			objG0.put("G0", gCode); arrGmodal.add(objG0);
			objT.put("T", tCode); arrAuxCode.add(objT);
			
			for(int i = 0 ; i < arrM.size() ; i++){
				if(i==0){
					objM1.put("M1", arrM.get(i)); arrAuxCode.add(objM1);
				}else if(i==1){
					objM2.put("M2", arrM.get(i)); arrAuxCode.add(objM2);
				}else if(i==2){
					objM3.put("M2", arrM.get(i)); arrAuxCode.add(objM3);
				}
			}

			rootObj.put("G_MODAL", arrGmodal);
			rootObj.put("AUX_CODE", arrAuxCode);
			
			return rootObj.toString();
		}

		@Override
		public String testDatetime2(AdapterVo inputVo){
			
			sql_ma.insert(ADAPTER_SPACE + "addAdtDatetime2Test",inputVo);
			
			return "OK";
		}

}
